# README #

### What is it? ###

This is a Contao extension to export Contao-calendars

* Right now it only exports complete (no added content elements) calendars to ICal (.ics) files.
* Calendars get exported by saving the export rule created by this extension (every time).
* If it is no single export (export once) every time a event of a calendar with an export rule gets saved it gets exported.
* Event title is used as SUMMARY and teaser is used as DESCRIPTION.
* It exports Title, Start Date/Time, End Date/Time, Location, Teaser and handles Recurrence settings (not well tested).


### Additional Information? ###

This extension uses Sabre\Vobject
https://github.com/fruux/sabre-vobject

If installed manually (without Composer) you will need to get it as well.
In this case you might want to follow these steps:

1. Download Sabre\Vobject e.g. 3.4.7 (Versions > 4 didn't work when this was written)
2. Add these files to the znrl_ics folder as following  
- create the folders "vendor/sabe/vobject" in system/modules/znrl_ics  
- copy all files from Sabre\Vobject (bin, lib, test, ... - "lib" might be the only one needed) to the new "vobject" folder
3. Get and install the NamespaceClassLoader (also available from [Github](https://github.com/terminal42/contao-NamespaceClassLoader)) only tested with version 1.0.2 from ER2
   https://contao.org/de/erweiterungsliste/view/NamespaceClassLoader.de.html
4. Replace the constructor in znrl_ics/classes/IcsExport.php with this

```
#!php
    public function __construct()
    {
        // Changed to work without Composer and use NamespaceClassloader instead to load Sabre\Vobject
        $loader = new \Composer\Autoload\ClassLoader();
        $loader->addPsr4('Sabre\\VObject\\', __DIR__.'/../vendor/sabre/vobject/lib');
        $loader->register();
    }

```

If you have troubles with special characters (google calendar)
it helped me to add following to the .htaccess:

```
<IfModule mod_mime.c>
  #...
  #other AddType rules by Contao
  #...
  AddType 'text/calendar; charset=UTF-8'      .ics
</IfModule>
```