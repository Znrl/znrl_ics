<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package   ZnrlIcs
 * @author    Lorenz Ketterer <lorenz.ketterer@web.de>
 * @license   GNU/LGPL
 * @copyright Lorenz Ketterer 2015
 */


/**
 * Back end modules
 */
$GLOBALS['TL_LANG']['MOD']['znrlics'] = array('Kalender -> Ics', 'Kalender in ics (ICal) Dateien exportieren');

