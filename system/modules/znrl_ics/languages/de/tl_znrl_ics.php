<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package   ZnrlIcs
 * @author    Lorenz Ketterer <lorenz.ketterer@web.de>
 * @license   GNU/LGPL
 * @copyright Lorenz Ketterer 2015
 */


/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_znrl_ics']['title'] = array('Titel', 'Bitte geben Sie einen Titel ein.');
$GLOBALS['TL_LANG']['tl_znrl_ics']['type'] = array('Art', 'Bitte wählen Sie ob ein Kalender importiert oder exportiert werden soll.');
$GLOBALS['TL_LANG']['tl_znrl_ics']['type']['export'] = 'Export';
$GLOBALS['TL_LANG']['tl_znrl_ics']['export_once'] = array('Einmalig exportieren', 'Der Kalender wird normalerweise jedesmal beim Speichern eines Kalenderevents erneut exportiert.');
$GLOBALS['TL_LANG']['tl_znrl_ics']['calendar'] = array('Kalender', 'Wählen Sie den Kalender aus, der exportiert werden soll.');
$GLOBALS['TL_LANG']['tl_znrl_ics']['export_unpublished'] = array('Unveröffentlichte Events Exportieren', 'Wählen Sie ob unveröffentlichte Events exportiert werden sollen oder nicht.');
$GLOBALS['TL_LANG']['tl_znrl_ics']['filename'] = array('Dateiname', 'Geben Sie einen Dateinamen OHNE die Endung .ics an.');
$GLOBALS['TL_LANG']['tl_znrl_ics']['file_destination_dir'] = array('Zielpfad', 'Geben sie an ob die Datei im Ordner /share oder Files-Verzeichnis gespeichert werden soll.');
$GLOBALS['TL_LANG']['tl_znrl_ics']['file_destination_dir']['files'] = 'Files-Verzeichnis';
$GLOBALS['TL_LANG']['tl_znrl_ics']['file_destination_dir']['share'] = '/share';
$GLOBALS['TL_LANG']['tl_znrl_ics']['file_destination_files'] = array('Pfad im Files-Verzeichnis', 'Geben sie den Pfad an im Files-Verzeichnis an.');


/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_znrl_ics']['title_legend'] = 'Titel und Art';
$GLOBALS['TL_LANG']['tl_znrl_ics']['export_legend'] = 'Kalender und Datei';


/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_znrl_ics']['new']    = array('Neu', 'Kalenderexport anlegen');
$GLOBALS['TL_LANG']['tl_znrl_ics']['show']   = array('Details', 'Details von ID %s anzeigen');
$GLOBALS['TL_LANG']['tl_znrl_ics']['edit']   = array('Bearbeiten ', 'Bearbeite ID %s');
$GLOBALS['TL_LANG']['tl_znrl_ics']['cut']    = array('Verschieben ', 'Verschiebe ID %s');
$GLOBALS['TL_LANG']['tl_znrl_ics']['copy']   = array('Duplizieren ', 'Dupliziere ID %s');
$GLOBALS['TL_LANG']['tl_znrl_ics']['delete'] = array('Löschen ', 'Lösche ID %s');
