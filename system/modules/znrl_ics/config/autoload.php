<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @package Znrl_ics
 * @link    https://contao.org
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */


/**
 * Register the namespaces
 */
ClassLoader::addNamespaces(array
(
    'Znrl',
));


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
    // Classes
    'Znrl\ZnrlIcs\IcsDca'       => 'system/modules/znrl_ics/classes/IcsDca.php',
    'Znrl\ZnrlIcs\IcsExport'    => 'system/modules/znrl_ics/classes/IcsExport.php',

    // Models
    'Znrl\ZnrlIcs\IcsModel' => 'system/modules/znrl_ics/models/IcsModel.php',
));
