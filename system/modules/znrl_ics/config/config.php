<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package   ZnrlIcs
 * @author    Lorenz Ketterer <lorenz.ketterer@web.de>
 * @license   GNU/LGPL
 * @copyright Lorenz Ketterer 2015
 */


/**
 * BACK END MODULES
 */

$GLOBALS['BE_MOD']['content']['znrlics'] = array(
    'tables'     => array('tl_znrl_ics'),
    'icon'       => 'system/modules/znrl_ics/assets/ics-import-export.png'
);


/**
 * MODEL MAPPINGS
 */

$GLOBALS['TL_MODELS']['tl_znrl_ics'] = 'Znrl\ZnrlIcs\IcsModel';
