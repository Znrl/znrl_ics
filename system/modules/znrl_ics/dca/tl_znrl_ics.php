<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package   ZnrlIcs
 * @author    Lorenz Ketterer <lorenz.ketterer@web.de>
 * @license   GNU/LGPL
 * @copyright Lorenz Ketterer 2015
 */


/**
 * Table tl_znrl_ics
 */
$GLOBALS['TL_DCA']['tl_znrl_ics'] = array
(

    // Config
    'config' => array
    (
        'dataContainer'               => 'Table',
        'enableVersioning'            => true,
        'onsubmit_callback'           => array
            (
                array('Znrl\ZnrlIcs\IcsDca', 'exportCalendarOnSave'),
            ),
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary'
            )
        )
    ),

    // List
    'list' => array
    (
        'sorting' => array
        (
            'mode'                    => 2,
            'fields'                  => array('title','type','calendar'),
            'flag'                    => 1,
            'panelLayout'             => 'filter;sort,search,limit'
        ),
        'label' => array
        (
            'fields'                  => array('title','type'),
            'format'                  => '%1$s - %2$s'
        ),
        'global_operations' => array
        (
            'all' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href'                => 'act=select',
                'class'               => 'header_edit_all',
                'attributes'          => 'onclick="Backend.getScrollOffset();" accesskey="e"'
            )
        ),
        'operations' => array
        (
            'edit' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_znrl_ics']['edit'],
                'href'                => 'act=edit',
                'icon'                => 'edit.gif'
            ),
            'copy' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_znrl_ics']['copy'],
                'href'                => 'act=copy',
                'icon'                => 'copy.gif'
            ),
            'delete' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_znrl_ics']['delete'],
                'href'                => 'act=delete',
                'icon'                => 'delete.gif',
                'attributes'          => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"'
            ),
            'show' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_znrl_ics']['show'],
                'href'                => 'act=show',
                'icon'                => 'show.gif'
            )
        )
    ),

    // Select
    'select' => array
    (
        'buttons_callback' => array()
    ),

    // Edit
    'edit' => array
    (
        'buttons_callback' => array()
    ),

    // Palettes
    'palettes' => array
    (
        '__selector__'                => array('type','file_destination_dir'),
        'default'                     => '{title_legend},title,type,export_once',
        'export'                      => '{title_legend},title,type,export_once;{export_legend},calendar,export_unpublished,filename,file_destination_dir'
    ),

    // Subpalettes
    'subpalettes' => array
    (
        'file_destination_dir_files'      => 'file_destination_files'
    ),

    // Fields
    'fields' => array
    (
        'id' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL auto_increment"
        ),
        'tstamp' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'title' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_ics']['title'],
            'exclude'                 => true,
            'search'                  => true,
            'sorting'                 => true,
            'flag'                    => 1,
            'inputType'               => 'text',
            'eval'                    => array('mandatory' => true, 'maxlength' => 255),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'type' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_ics']['type'],
            'exclude'                 => true,
            'sorting'                 => true,
            'filter'                  => true,
            'inputType'               => 'select',
            'options'                 => array('export'),
            'reference'               => &$GLOBALS['TL_LANG']['tl_znrl_ics']['type'],
            'eval'                    => array('mandatory' => true, 'includeBlankOption' => true, 'submitOnChange' => true),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'export_once' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_ics']['export_once'],
            'exclude'                 => true,
            'sorting'                 => true,
            'filter'                  => true,
            'inputType'               => 'checkbox',
            'eval'                    => array('tl_class' => 'm12 cbx', 'submitOnChange' => true),
            'sql'                     => "char(1) NOT NULL default ''"
        ),
        'calendar' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_ics']['calendar'],
            'exclude'                 => true,
            'search'                  => true,
            'sorting'                 => true,
            'inputType'               => 'select',
            'options_callback'        => array('Znrl\ZnrlIcs\IcsDca', 'getCalendars'),
            'eval'                    => array('tl_class' => 'w50', 'mandatory' => true, 'multiple' => false),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'export_unpublished' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_ics']['export_unpublished'],
            'exclude'                 => true,
            'inputType'               => 'checkbox',
            'eval'                    => array('tl_class' => 'w50 m12 cbx'),
            'sql'                     => "char(1) NOT NULL default ''"
        ),
        'filename' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_ics']['filename'],
            'exclude'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('tl_class' => 'clr', 'mandatory' => true, 'maxlength' => 255),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'file_destination_dir' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_ics']['file_destination_dir'],
            'exclude'                 => true,
            'inputType'               => 'radio',
            'eval'                    => array('submitOnChange' => true),
            'options'                 => array('files', 'share'),
            'reference'               => &$GLOBALS['TL_LANG']['tl_znrl_ics']['file_destination_dir'],
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'file_destination_files' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_ics']['file_destination_files'],
            'exclude'                 => true,
            'inputType'               => 'fileTree',
            'eval'                    => array('fieldType' => 'radio', 'mandatory' => true),
            'sql'                     => "binary(16) NULL",
        ),
    )
);
