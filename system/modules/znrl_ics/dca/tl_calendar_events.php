<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package   ZnrlIcs
 * @author    Lorenz Ketterer <lorenz.ketterer@web.de>
 * @license   GNU/LGPL
 * @copyright Lorenz Ketterer 2015
 */


/**
 * Table tl_calendar_events
 */

$GLOBALS['TL_DCA']['tl_calendar_events']['config']['onsubmit_callback'][] = array('Znrl\ZnrlIcs\IcsExport', 'exportCalendar');